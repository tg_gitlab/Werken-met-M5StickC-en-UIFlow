#  Werken met M5StickC en UIFlow
_Snel en eenvoudig een Internet of Things toepassing maken met M5StickC, Python en UI Flow_<img src="/artwork/m5stickc-specs.png" width="350" align="right">

De M5StickC is een op de Espressif ESP32 gebaseerd klein, flexibel computertje met kleurenbeeldscherm in een in het oog springende oranje behuizing. De 'stick' is eenvoudig via wifi met het internet te verbinden en heeft veel mogelijkheden. Het heeft ingebouwde schakelaars en sensoren, een 80 mAh accu, leds, een sensorpoort, een uitbreidingspoort, bluetooth en nog veel meer. De afmetingen van de M5StickC zijn ongeveer 5 x 2,5 x 1,5 cm. 

Je kunt de M5StickC in Arduino of Lua programmeren. De Arduino programmeertaal wordt volledig door de M5StickC ondersteund, met hierbij passende codebibliotheken. In deze beschrijving gaan we echter uit van een andere programmeertaal, namelijk een combinatie van [Python](https://nl.wikipedia.org/wiki/Python_(programmeertaal)) en [Google Blockly](https://developers.google.com/blockly/guides/overview), genaamd UI Flow.

## Wat heb je nodig
* Een [M5StickC](https://www.okaphone.com/artikel.asp?id=490779) ('ESP32 IoT ontwikkel board met LCD') voorzien van MicroPython en UI Flow firmware (zie [hier](#firmware-upgraden) de beschrijving om de juiste firmware te laden)
* Een USB-C data- en laadkabeltje (er wordt eentje meegeleverd met de M5StickC)
* Een internet browser, bijvoorbeeld op je telefoon of laptop
* Beschikking over een wifi toegangspunt

Optioneel kun je nog gebruik maken van [één van de vele uitbreidingen](https://www.okaphone.com/artikelen.asp?groep=1573) die er voor de M5StickC verkrijgbaar zijn en die vanuit UI Flow met bijbehorende codeblokken kunnen worden gebruikt.

Voor de officiële documentatie voor de M5StickC is een speciale website beschikbaar op de website van de leverancier: [M5StickC Documentation](https://docs.m5stack.com/#/en/core/m5stickc). Hiernaast is er een GitHub repository waarop de laatste versie van de firmware en Python modules te vinden is: [M5StickC GitHub](https://github.com/m5stack/M5StickC).

### Beschrijving van het blokschema van de M5StickC
De M5StickC is voorzien van een microcontroller van het type ESP32 met ingebouwd 4 MB flashgeheugen voor de firmware en gebruikersprogramma's. Het geheugen is groot genoeg voor tientallen programma's en data die door een toepassing wordt opgeslagen. De firmware is voorzien van een menu waarmee de verschillende programma's kunnen worden gestart, maar programma's kunnen elkaar ook aanroepen.

<img alt="Blokschema van de M5StickC" src="/artwork/m5stickc-blokschema.png" width="350">

De stroomvoorziening van de M5StickC heeft een aparte coprocessor, in de vorm van een AXP192 accumanager. Deze regelt het laden en ontladen van de ingebouwde accu, maar bijvoorbeeld ook de stroomvoorziening naar het display. De USB aansluiting wordt gebruikt voor het laden van de accu en voor het vervangen van de firmware. Er zijn verschillende ingebouwde sensoren: een SH200Q axelerometer en gyroscoop, een SPM1423 microfoon, een temperatuursensor in de AXP192 accumanager en drie schakelaars. Een rode led, een infrarood-led en een 80x160 pixel display met achtergrondverlichting vormen de ingebouwde actuatoren.

### M5StickC verbinden met het internet
<img src="/artwork/uiflow-verbindingsscherm.jpeg" width="80" align="right">Na het voor de eerste keer inschakelen van de M5StickC wordt het toegangspunt-identificatiescherm getoond. Het tonen van dit scherm geeft aan dat de M5StickC geen wifi toegangspunt kon vinden om mee te verbinden en nu zelf een wifi toegangspunt is geworden. De naam van dit toegangspunt wordt op het scherm afgebeeld (in het voorbeeld 'M5-682c', maar voor iedere M5StickC verschillend). Maak met computer, tablet of smartphone verbinding met dit toegangspunt en ga met de browser naar adres '192.168.4.1'. Op deze pagina kan een wifi toegangspunt in de buurt worden gekozen, en het bijbehorende wachtwoord worden ingevoerd. Hierna herstart de M5StickC en wordt verbinding met het ingestelde wifi toegangspunt gemaakt.

### Opstartmenu
<img src="/artwork/uiflow-startup.jpeg" width="120" align="right">Direct na het opstarten kan op de 'M5' knop worden gedrukt om een menu op te roepen. Met dit menu kan het wifi toegangspunt worden veranderd, maar kunnen er ook reeds geladen programma's worden opgestart. Met de knop aan de zijkant van het display wordt steeds de volgende optie gekozen; met de 'M5' knop wordt een keuze bevestigd.

### Werken met UI Flow
UI Flow is [een online codeerplatform](flow.m5stack.com), de programma's die je in UI FLow maakt worden in via het internet naar je M5StickC verzonden en daar in Python uitgevoerd.
1. **Codeergebied**: hier plaats je de codeblokken waarmee je je programma maakt. Codeblokken die je niet meer nodig hebt kun je naar de afbeelding van de vuilnisemmer verslepen.
2. **Codeblokken**: de codeblokken die samen je programma vormen. Ieder programma begint met een Setup codeblok. Het Loop codeblok wordt eindeloos herhaald.
3. **Blokkenlijst**: dit zijn de bouwblokken waarmee een programma wordt gemaakt. De codeblokken 'klikken' in- en aanelkaar en laten op die manier zien dat de structuur van een programma goed is. Ieder codeblok heeft 'onder de motorkap' Python instructies die op de M5StickC worden uitgevoerd.
4. **UI ontwerphulp**: hiermee kunnen grafische elementen op het beeldscherm worden geplaatst, die vanuit de codeblokken kunnen worden bestuurd.
5. **Teken gereedschap**: de grafische elementen die op het beeldscherm kunnen worden geplaatst.
6. **Extra programmeerblokken**: hardware uitbreidingen worden ondersteund met extra codeblokken.
7. **Menubalk**: hier start je je programma, upload je grafische elementen naar je M5StickC en kun je je programma naar je lokale computer downloaden.
8. **Weergave en verwijderen codeblokken**: met deze knoppen kun je de weergave van je programma vergroten en verkleinen. Codeblokken die in het codeergebied in de weg staan kun je in de prullebak slepen.
9. **Statusbalk**: hier zie je de verbindingsstatus met je M5StickC. Klik op de statusbalk om de instellingen van je M5StickC te veranderen.

<img alt="UI Flow programmeeromgeving" src="/artwork/uiflow-screen-explanation.png" width="500">

## Projecten om mee van start te gaan

### Hallo Groningen
<img src="/artwork/uiflow-hallo-groningen.png" width="350" align="right">Een eerste vingeroefening met UI Flow kan de traditionele [hello, world](https://nl.wikipedia.org/wiki/Hello_world_(programma)) zijn, een programma dat niets anders doet dan een welkomstboodschap op het display tonen. Met dit programma toon je aan dat alle onderdelen het doen en dat UI Flow, je M5StickC en jijzelf klaar zijn voor het grotere werk.

Sleep vanuit de blokkengroep `UI` het codeblok `Set screen rotation mode` naar het codeergebied en klik dit blok aan het al aanwezige blok `Setup` vast. Stel dit blok in op `3`. Haal uit `Graphic` het blok `Font` en stel deze in op `FONT_DejaVu72`. Haal uit `Graphic` tevens `Lcd.print`. Stel hier de tekst in en zet de X en Y waarden op resp. 10 en 5. Je programma moet er nu uitzien zoals in het voorbeeld wordt weergegeven. Druk nu op de afspeelknop en bekijk de tekst op het scherm van je M5StickC. Gelukt? Van harte gefeliciteerd met je eerste UI FLow programma! Geen tekst? Waarschijnlijk past jouw tekst dan niet op het scherm. Kies een kleiner lettertype of maak je tekst korter zodat deze wel in z'n geheel past.

### De ledlantaarn
<img src="/artwork/uiflow-ledlantaarn.png" width="200" align="right">Met de ingebouwde rode led kun je een handige zaklantaarn maken. In dit programma werken we met <i>events</i>, gebeurtenissen, om vast te stellen dat op een knop is gedrukt. 

Maak het codeergebied leeg (sleep eventueel aanwezige codeblokken naar de vuilnisemmer) en sleep vanuit de blokkengroep `Event` het codeblok `Button` naar het codeergebied. Het codeblok staat op zichzelf en kan niet verbinden met `Setup`. De codeblokken in deze gebeurtenis worden aangeroepen zodra aan de voorwaarden van de gebeurtenis worden voldaan. Stel `Button` in op `A` en `wasPressed`. Haal uit `Hardwares`>`LED` het blok `LED ON` en plaats deze in de gebeurtenis `Button A wasPressed`. Maak nu een gebeurtenis `Button A wasReleased` en plaats hierin het blok `LED OFF`. Druk op de afspeelknop en probeer je nieuwe ledlantaarn uit: druk op de M5-knop om de led in te schakelen en laat de knop weer los om de led uit te schakelen. De accu van de M5StickC gaan met deze toepassing niet heel lang mee, dus echt handig is deze zaklantaarn niet. Maar een leuke gebeurtenisgedreven toepassing is het wel.

### Digitale hardware aansturen
<img src="/artwork/m5stickc-led-aansluiten.png" width="125" align="left">De M5StickC heeft een poort waarop uitbreidingen ('HATs') kunnen worden aangesloten. Op deze poort kun je echter ook je eigen hardware aansluiten. Er staan drie bestuurbare pinnen tot je beschikking (`G26`, `G36` en `G0`), die je kunt gebruiken als uitgang, ingang en als communicatielijn. Verder vind je op de poort nog vijf spanningsaansluitingen. [Een kleurenled](https://www.okaphone.com/artikel.asp?id=456482) kan direct op de poort worden aangesloten, door de lange draad in aansluiting `G26` en de korte draad in `GND` te drukken.

Aansluiting `G26` kan met codeblok `digital write pin` worden in- en uitgeschakeld. Dit codeblok vind je in blokkengroep `Advanced`>`Easy I/O`. Met `digital write pin 26 value 1` schakel je de aangesloten led in en met `digital write pin 26 value 0` schakel je hem weer uit.

### Dobbelstick
Een grappige toepassing van de M5StickC is het gebruik als dobbelsteensimulator. Met een druk op de knop worden twee dobbelstenen geworpen. Na enkele seconden blijven de ogen boven liggen.

Met het voorbeeldprogramma [`throw_dice.m5f`](throw_dice.m5f) is de dobbelsteensimulator uit te voeren. Laad dit programma in de UI FLow omgeving en upload het naar de M5StickC.

## Gebruik maken van de kracht van het internet
Je M5StickC kan heel eenvoudig een webpagina opvragen met het codeblok `HttpRequest` (je vindt dit codeblok wat weggestopt in de blokkengroep `Advanced`>`Http`). Met de `Method` ingesteld op `GET` wordt de webpagina die is ingesteld bij `URL` opgeroepen. Sommige webpagina's voeren taken voor je uit, voordat ze de pagina tonen. Ze kunnen bijvoorbeeld een twitterbericht voor je versturen, of namens jou een email verzenden. De webpagina die dan wordt teruggegeven bevat niet heel veel tekst, meestal niet meer dan 'Okee, heb ik voor je geregeld'.

### If This Then That
<img src="/artwork/ifttt-applet-lifx.png" width="200" align="left">Zo'n taak-uitvoerende-website is het gratis <a href="http://www.ifttt.com">IFTTT.com</a>, wat voor _IF_ _This_ _Then_ _That_ staat ("als dit, dan dat"). Je kunt met IFTTT instellen dat als je er een speciale pagina (je noemt dat een _websocket_) opvraagt, IFTTT voor jou een activiteit uitvoert. Je maakt, zoals IFTTT het noemt, een _applet_. Het adres van die speciale pagina neem je over in de `URL` van codeblok `HttpRequest`. Nu kan je M5StickC op ieder moment die applet aanroepen, bijvoorbeeld als er een nieuwe sensorwaarde beschikbaar is of als er op een knop wordt gedrukt. Je kunt die sensorwaarde zelfs meegeven bij het aanroepen van de speciale pagina. Zo kun je bijvoorbeeld een Google spreadsheet met sensorwaarden laten vullen, of IFTTT een slimme thermostaat laten aansturen.

Om gebruik te kunnen maken van IFTTT moet je een [account](https://ifttt.com/join) aanmaken. Dat kan met Google of Facebook, maar evengoed maak je een account aan met je emailadres en een (veilig gekozen) wachtwoord. Nadat je bent ingelogd op IFTTT kun je gelijk applets aanmaken met internetdiensten waar je gebruik van maakt. Er zijn honderden diensten waar je uit kunt kiezen, waaronder Facebook, Twitter, Dropbox, Gmail, Google Drive (Docs, Spreadsheet), OneDrive, RSS, Tumbler, YouTube, OneNote, Instagram, FourSquare, Google Calendar, Weather, SMS en Webhooks. Die laatste is voor ons handig; hiermee verstuur je opdrachten vanuit je M5StickC die door IFTTT met de andere diensten kunnen worden verbonden.

<img src="/artwork/ifttt-step1.png" width="500"><br>
<img src="/artwork/ifttt-step2.png" width="500">

Om een applet bij IFTTT aan te maken doorloop je de afgebeelde stappen:
1. **Create your own**: maak een nieuwe applet (je vindt deze optie bij `Account`)
2. **Choose a service**: kies `Webhook` als triggerende service
3. **Choose a trigger**: kies `Recieve a web request` als trigger
4. **Complete trigger fields**: voer hier een zelfgekozen naam voor je gebeurtenis in
5. **Choose action service**: kies een service die moet reageren op je trigger
6. **Webhook service key**: ga naar `My services`>`Webhooks`>`Settings` en kopieer de getoonde sleutel

### Internetlamp aansturen
<img src="/artwork/uiflow-ifttt-webhook-applet.png" width="350" align="right">Met dit project gebruik je een IFTTT applet om een LIFX internetlamp aan te sturen met je M5StickC. Je kunt dit project als basis gebruiken voor je eigen internet of things toepassingen die van IFTTT gebruik moeten maken.

Download eerst het voorbeeldprogramma [`lifx_lamp.m5f`](lifx_lamp.m5f) en open dit met UI Flow. Maak nu twee applets in IFTTT aan met de stappen die hierboven zijn beschreven: de naam voor de eerste trigger wordt `lamp_aan`, de naam voor de tweede wordt `lamp_uit`. Voor beide applets kies je als `action service` voor `LIFX`. Bij `lamp_aan` stel je voor de `LIFX` service in dat de lamp aan gaat, op de manier en met de kleur die jij wilt. Bij de trigger `lamp_uit` stel je in dat de lamp uit gaat.

Om het programmavoorbeeld werkend te krijgen dien je je eigen `Webhook` sleutel te gebruiken. Deze sleutel krijgt je van IFTTT als je voor de eerste keer een applet maakt met de `Webhook` service. Zie stap 6 van de beschrijving hierboven als je deze sleutel nog niet hebt.

Het programmavoorbeeld maakt gebruik van verschillende handige technieken om overzichtelijke code te maken. Zo staan de twee functies `to zet_lamp_aan` en `to zet_lamp_uit` centraal en wordt gebruik gemaakt van twee variabelen `ifttt_webhook_key` en `lamp_status`. Deze laatste wordt gebruikt om bij te houden of de lamp aan- of uitstaat, om op die manier tussen deze twee toestanden te kunnen schakelen. Je ziet in de gebeurtenis `Button A wasPressed` dan ook het codeblok `if lamp_status = "uit"` staan. Afhankelijk van de waarde van `lamp_status` wordt op deze manier één van de twee functies aangeroepen.

De aanroep van de IFTTT applets wordt met de codeblokken `Http Request` uitgevoerd. Beide blokken zijn nagenoeg identiek ingevuld, op de naam van de triggerende functie na: de ene request heeft `lamp_aan` als trigger en de andere heeft `lamp_uit` als trigger. In beide gevallen wordt de variabele `ifttt_webhook_key` meegegeven aan de URL zodat IFTTT weet dat jij het bent. Zorg ervoor dat je in het codeblok `set ifttt_webhook_key to` je eigen sleutel hebt ingevuld, anders krijg je geen toegang tot je eigen `Webhook` service. De sleutel is voor al jouw applets die gebruik maken van de `Webhook` service identiek.

## Firmware upgraden
<img src="/artwork/M5Burner.png" width="350" align="right">De M5StickC is af fabriek voorzien van een testprogramma dat een M5Stack logo laat zien en waarmee een timer en wifi toegangspunten in de buurt worden getoond. Hiermee kan worden vastgesteld dat de M5StickC werkt, maar er kan niet mee met UI Flow worden gewerkt. 

Om gebruik te kunnen maken van UI Flow is het nodig om de M5StickC te voorzien van MicroPython firmware. Deze firmware wordt door M5Stack, de makers van de M5StickC, onderhouden. Om de MicroPython firmware in de M5StickC te laden is er een handig programma genaamd M5Burner, beschikbaar voor Windows en Mac OS. Download het M5Burner programma vanaf [de downloadpagina van M5Stack](https://m5stack.com/pages/download), of vanuit het `Settings` scherm in UI Flow zelf.

Doorloop de volgende stappen om met M5Burner de laatste versie van de firmware in je M5StickC te laden:
1. **Download de firmware**: zoek in de lijst beschikbare firmwares de meeste recente versie. Op moment van schrijven is dit `UIFlow-v1.4.2`, maar dit zal vrij snel een hogere versie zijn. Klik op de downloadknop gelijk rechts naast de naam van de firmware om de firmware op je computer te laden. De downloadvoortgang wordt met een groen balkje weergegeven
2. **Sluit je M5StickC aan op de computer**: kies de poort waarop je M5StickC is aangesloten uit de `COM:` lijst. Op een Windows computer is dat iets van `COM<i>n</i>`, op een Mac is dat `/dev/tty.usbserial<i>xyz</i>`.  Kies voor `Baudrate:` de waarde `750000`. Kies `StickC` in het lijstje `Series:`
3. **Laad de firmware**: klik op `Burn` om de firmware naar je M5StickC te versturen. Dit neemt een paar minuten in beslag. Je ziet tijdens het 'branden' allerlei technische informatie op het scherm getoond worden. Dit kun je veilig negeren

Na het 'branden' van de nieuwe firmware moet je je M5StickC met de hand uit- en weer aanzetten.

## Problemen oplossen
De M5StickC is een stabiel en betrouwbaar apparaatje dat voorspelbare resultaten oplevert. Soms zit er echter wat tegen. Hieronder volgt een lijstje aandachtspunten voor als het allemaal niet werkt zoals je dat wilt:
* De M5StickC heeft niet de juiste firmware<br>
Je kunt de firmware van je M5StickC relatief eenvoudig veranderen met het programma M5Burner. [Hier](#firmware-upgraden) wordt beschreven hoe dat moet.
* Je krijgt je M5StickC niet verbonden met een wifi toegangspunt<br>
Als je WiFi-netwerk spaties in de SSID of in het wachtwoord heeft, kijk dan even naar de URL die de browser gebruikt voor het toevoegen van het wifi toegangspunt. Die is van de vorm `http://192.168.4.1/configure?ssid=mijn+netwerk&password=mijn+geheime+wachtwoord`. Dat werkt niet, maar als je de plussen vervangt door `%20` dan werkt het wel. Dus: `http://192.168.4.1/configure?ssid=mijn%20netwerk&password=mijn%20geheime%20wachtwoord` (dank Hans Marks)
* M5Burner doet het niet goed<br>
  * Op een Mac is het nodig om het M5Burner programma naar de Programma's of Apps map te verplaatsen. Het lijkt niet goed te werken in de Downloads map.
  * Onder Windows is het soms nodig het M5Burner.zip bestand te ontgrendelen: Rechtermuis op het zipbestand `M5Burner.zip`, kies voor properties (eigenschappen) van het bestand, plaats een vinkje in de checkbox van `Unblock` en klik op `OK` (dank Alex Kuiper)
* Het `Lcd.print` codeblok toont geen tekst op het scherm<br>
Als de tekst niet geheel op het scherm past dan wordt de tekst niet afgedrukt. Maak dan de tekst korter, wijzig de coordinaten of maak de letters kleiner.
* Het scherm licht grijs op, maar verder lijkt er niets meer te gebeuren<br>
Als een programma geen uitvoer naar het schermpje heeft, dan zal het schermpje lichtgrijs oplichten. Je kunt een nieuw programma naar je M5StickC sturen met UI Flow, of de M5StickC met de hand uit- en weer aanzetten (houd de schakelaar aan de zijkant bij de M5 knop 6 seconden ingedrukt om de M5StickC uit te schakelen. Druk vervolgens kort op deze knop om de M5StickC weer aan te zetten.)

## Hall of Fame
* Flappy Bird in UI Flow<br>
Spel waarin je een balletje in de lucht moet houden met de M5 knop.